//
// Created by twak on 11/11/2019.
//

#include "the_button.h"


void TheButton::init(TheButtonInfo* i) {
    setIcon( *(i->icon) );
    info =  i;
    duration = ((rand()%7)+2)*10;
    date = QDate(2020,11,16);
}

void TheButton::clicked() {
    if(type==0){
        emit addToTimeline(info,duration);
    } else if(type==1){
        emit removeFromTimeline(id);
    } else if(type == 2 && accessible ==1  ){ //when the theme button is pushed change the colours of the stored "widgets"

        w->setStyleSheet("background-color:#012615");
        f->setStyleSheet("background-color:#ccf7ba;");
        cho->setStyleSheet("background-color:#ccf7ba;");
        cont->setStyleSheet("background-color:#ccf7ba;");

        il->setStyleSheet("background-color:#02734A;");
        tl->setStyleSheet("background-color:#02734A;");

        prb->setStyleSheet("background-color:#05A66B;");
        pb->setStyleSheet("background-color:#05A66B;");
        nb->setStyleSheet("background-color:#05A66B;");
        mb->setStyleSheet("background-color:#05A66B;");
        bb->setStyleSheet("background-color:#05A66B;");
        this->setStyleSheet("background-color:#05A66B;");

        accessible=0;
    } else if(type == 2 && accessible ==0  ){

        w->setStyleSheet("background-color:#011226;color:#00050d;");
        f->setStyleSheet("background-color:#baeff7;");
        cho->setStyleSheet("background-color:#baeff7;");
        cont->setStyleSheet("background-color:#baeff7;");

        il->setStyleSheet("background-color:#022673;color:white;");
        tl->setStyleSheet("background-color:#022673;color:white;");

        prb->setStyleSheet("background-color:#1E88E5;");
        pb->setStyleSheet("background-color:#1E88E5;");
        nb->setStyleSheet("background-color:#1E88E5;");
        mb->setStyleSheet("background-color:#1E88E5;");
        bb->setStyleSheet("background-color:#1E88E5;");
        this->setStyleSheet("background-color:#1E88E5;");
        accessible=1;

    }


}


