/*
 *
 *    ______
 *   /_  __/___  ____ ___  ___  ____
 *    / / / __ \/ __ `__ \/ _ \/ __ \
 *   / / / /_/ / / / / / /  __/ /_/ /
 *  /_/  \____/_/ /_/ /_/\___/\____/
 *              video for sports enthusiasts...
 *
 *  2811 cw3 : twak
 */

#include <iostream>
#include <QApplication>
#include <QtMultimediaWidgets/QVideoWidget>
#include <QMediaPlaylist>
#include <string>
#include <vector>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QHBoxLayout>
#include <QtCore/QFileInfo>
#include <QtWidgets/QFileIconProvider>
#include <QDesktopServices>
#include <QImageReader>
#include <QMessageBox>
#include <QtCore/QDir>
#include <QtCore/QDirIterator>
#include "the_player.h"
#include "the_button.h"
#include <QScrollArea>
#include <QLabel>
#include <QMediaObject>
#include <QMediaMetaData>

using namespace std;

// read in videos and thumbnails to this directory
vector<TheButtonInfo> getInfoIn (string loc) {

    vector<TheButtonInfo> out =  vector<TheButtonInfo>();
    QDir dir(QString::fromStdString(loc) );
    QDirIterator it(dir);

    while (it.hasNext()) { // for all files

        QString f = it.next();

            if (f.contains("."))

#if defined(_WIN32)
            if (f.contains(".wmv"))  { // windows
#else
            if (f.contains(".mp4") || f.contains("MOV"))  { // mac/linux
#endif

            QString thumb = f.left( f .length() - 4) +".png";
            if (QFile(thumb).exists()) { // if a png thumbnail exists
                QImageReader *imageReader = new QImageReader(thumb);
                    QImage sprite = imageReader->read(); // read the thumbnail
                    if (!sprite.isNull()) {
                        QIcon* ico = new QIcon(QPixmap::fromImage(sprite)); // voodoo to create an icon for the button
                        QUrl* url = new QUrl(QUrl::fromLocalFile( f )); // convert the file location to a generic url
                        out . push_back(TheButtonInfo( url , ico  ) ); // add to the output list
                    }
                    else
                        qDebug() << "warning: skipping video because I couldn't process thumbnail " << thumb << endl;
            }
            else
                qDebug() << "warning: skipping video because I couldn't find thumbnail " << thumb << endl;
        }
    }

    return out;
}

int main(int argc, char *argv[]) {
    // let's just check that Qt is operational first
    qDebug() << "Qt version: " << QT_VERSION_STR << endl;
    // create the Qt Application
    QApplication app(argc, argv);
    // collect all the videos in the folder
    vector<TheButtonInfo> videos;
    if (argc == 2)
        videos = getInfoIn( string(argv[1]) );

    if (videos.size() == 0) {
        const int result = QMessageBox::question(
                    NULL,
                    QString("Tomeo"),
                    QString("no videos found! download, unzip, and add command line argument to \"quoted\" file location. Download videos from Tom's OneDrive?"),
                    QMessageBox::Yes |
                    QMessageBox::No );
        switch( result )
        {
        case QMessageBox::Yes:
          QDesktopServices::openUrl(QUrl("https://leeds365-my.sharepoint.com/:u:/g/personal/scstke_leeds_ac_uk/EcGntcL-K3JOiaZF4T_uaA4BHn6USbq2E55kF_BTfdpPag?e=n1qfuN"));
          break;
        default:
            break;
        }
        exit(-1);
    }

    // the widget that will show the video
    QVideoWidget *videoWidget = new QVideoWidget;

    // the QMediaPlayer which controls the playback
    ThePlayer *player = new ThePlayer;
    player->setVideoOutput(videoWidget);

    // a column of buttons
    QWidget *filesWidget = new QWidget();
    vector<TheButton*> files;
    // the buttons are arranged vertically
    QVBoxLayout *filesLayout = new QVBoxLayout();
    filesWidget->setLayout(filesLayout);
    // create the buttons
    for ( int i = 0; i < int(videos.size()); i++ ) {
        TheButton *button = new TheButton(filesWidget,i,0);
        button->connect(button, SIGNAL(addToTimeline(TheButtonInfo*,int)), player, SLOT (addToTimeline(TheButtonInfo*,int))); // when clicked, tell the player to play.
        files.push_back(button);
        button->init(&videos.at(i));
        QLabel *videoDate = new QLabel(files.at(i)->date.toString("yyyy.MM.dd"));
        filesLayout->addWidget(videoDate);
        filesLayout->addWidget(button);
    }

    QScrollArea *fileScroll = new QScrollArea();
    fileScroll->setWidget(filesWidget);
    fileScroll->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    fileScroll->setMinimumWidth(250);
    fileScroll->setMaximumWidth(250);

    //new layout to add title above the video scroll area
    QVBoxLayout *importedLayout = new QVBoxLayout();
    QLabel *importedLabel = new QLabel("Imported Files:");
    importedLayout->addWidget(importedLabel);
    importedLayout->addWidget(fileScroll);
    importedLabel->setStyleSheet("background-color:#05A66B");


    //timeline creation
    QWidget *chosenWidget = new QWidget();
    QHBoxLayout *chosenLayout = new QHBoxLayout();
    chosenWidget->setLayout(chosenLayout);
    // tell the player what buttons and videos are available
    player->setContent(&files, &videos,chosenLayout,chosenWidget);
    QScrollArea *chosenScroll = new QScrollArea();
    chosenScroll->setWidget(chosenWidget);
    chosenScroll->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    chosenScroll->setMinimumHeight(150);
    chosenScroll->setMaximumHeight(150);

    //create timeline layout to add a label above the timeline
    QVBoxLayout* timelineLayout = new QVBoxLayout();
    QLabel *timelineLabel = new QLabel("Timeline:");
    timelineLabel->setStyleSheet("background-color:#05A66B;");
    timelineLabel->setMaximumHeight(25);
    timelineLabel->setMinimumHeight(25);
    timelineLayout->addWidget(timelineLabel);
    timelineLayout->addWidget(chosenScroll);


    //create layout for the media controls
    QHBoxLayout *controlLayout = new QHBoxLayout();
    //create previous button
    QPushButton *prevButton = new QPushButton();
    prevButton->setMinimumSize(QSize(70,70));
    prevButton->setIcon(QIcon("../uicw3/prev.png"));
    prevButton->setIconSize(QSize(50,50));
    prevButton->setStyleSheet("background-color: #05A66B");
    prevButton->connect(prevButton, SIGNAL(clicked()), player, SLOT (playPrev())); // when clicked, tell the player to go tp prev video.

    //create play button
    QPushButton *playButton = new QPushButton();
    playButton->setMinimumSize(QSize(70,70));
    playButton->setIcon(QIcon("../uicw3/play.png"));
    playButton->setIconSize(QSize(50,50));
    playButton->setStyleSheet("background-color: #05A66B");
    playButton->connect(playButton, SIGNAL(clicked()), player, SLOT (playPause())); // when clicked, tell the player to play.

    //create next button
    QPushButton *nextButton = new QPushButton();
    nextButton->setMinimumSize(QSize(70,70));
    nextButton->setIcon(QIcon("../uicw3/next.png"));
    nextButton->setIconSize(QSize(50,50));
    nextButton->setStyleSheet("background-color: #05A66B");
    nextButton->connect(nextButton, SIGNAL(clicked()), player, SLOT (playNext())); // when clicked, tell the player to go to next video.

    //create layout for controls that are vertical
    QVBoxLayout *verticalControls = new QVBoxLayout();

    //create mute button
    QPushButton *muteButton = new QPushButton();
    muteButton->setMinimumSize(QSize(50,50));
    muteButton->setIcon(QIcon("../uicw3/mute.png"));
    muteButton->setIconSize(QSize(40,40));
    muteButton->setStyleSheet("background-color: #05A66B");
    muteButton->connect(muteButton, SIGNAL(clicked()), player, SLOT (toggleAudio()));

    //create erase all button
    QPushButton *binButton = new QPushButton();
    binButton->setMinimumSize(QSize(50,50));
    binButton->setIcon(QIcon("../uicw3/bin.png"));
    binButton->setIconSize(QSize(40,40));
    binButton->setStyleSheet("background-color: #05A66B");
    binButton->connect(binButton, SIGNAL(clicked()), player, SLOT (removeAllFromTimeline())); // when clicked, tell the player to go to next video.

    verticalControls->addWidget(muteButton);
    verticalControls->addWidget(binButton);

    controlLayout->addWidget(prevButton);
    controlLayout->addWidget(playButton);
    controlLayout->addWidget(nextButton);
    controlLayout->addLayout(verticalControls);
    //widget to hold the controls so that we can style the background
    QWidget *controlWindow = new QWidget();
    controlWindow->setLayout(controlLayout);


    // create the main window and layout
    QWidget window;
    QHBoxLayout *mainLayout = new QHBoxLayout();
    QVBoxLayout *editorLayout = new QVBoxLayout();
    QHBoxLayout *bottomLayout= new QHBoxLayout();

    //create theme button here so that all the "widgets" we want to change colour have already been defined.
    QPushButton *themeButton = new TheButton(2,&window,controlWindow,fileScroll,chosenScroll,importedLabel,timelineLabel,prevButton,playButton,nextButton,muteButton,binButton);
    themeButton->setMinimumSize(QSize(50,50));
    themeButton->setIcon(QIcon("../uicw3/night.png"));
    themeButton->setIconSize(QSize(40,40));
    themeButton->setStyleSheet("background-color: #05A66B");
    verticalControls->addWidget(themeButton);

    window.setLayout(mainLayout);
    window.setWindowTitle("tomeo");
    window.setMinimumSize(1050, 640);

    //add controls and timeline to bottom layout
    bottomLayout->addWidget(controlWindow);
    bottomLayout->addLayout(timelineLayout);
    bottomLayout->setStretch(0,2);
    bottomLayout->setStretch(1,3);

    // add the video and the buttons to the editor layout
    editorLayout->addWidget(videoWidget);
    editorLayout->addLayout(bottomLayout);

    // add the editor and file layout to main layout
    mainLayout->addLayout(importedLayout);
    mainLayout->addLayout(editorLayout);

    // set colors
    fileScroll->setStyleSheet("background-color:#ccf7ba;");
    chosenScroll->setStyleSheet("background-color:#ccf7ba;");
    window.setStyleSheet("background-color:#012615;color:#000D06;");
    controlWindow->setStyleSheet("background-color:#ccf7ba;");


    // showtime!

    window.show();

    // wait for the app to terminate
    return app.exec();
}
