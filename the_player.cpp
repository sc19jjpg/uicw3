//
// Created by twak on 11/11/2019.
//

#include "the_player.h"

using namespace std;

// all buttons have been setup, store pointers here
void ThePlayer::setContent(std::vector<TheButton*>* b, std::vector<TheButtonInfo>* i,QHBoxLayout *l,QWidget *w) {
    buttons = b;
    infos = i;
    chosenLayout=l;
    chosenWidget=w;
}


//when a video ends playing, go to the next one on the timeline
void ThePlayer::playStatusChanged (QMediaPlayer::MediaStatus ms) {
    switch (ms) {
        case 7: //QMediaPlayer::EndOfMedia
            playNext(); // play the next clip in the timeline
            break;
    default:
        break;
    }
}

//play the next video by cheking if the next is the first one or really the next one
void ThePlayer::playNext(){
    if(int(timeline.size())>0){
        timeline.at(playingIndex)->setStyleSheet("background-color:#02734A;");
        if(playingIndex< int(timeline.size()-1)){
            playingIndex++;
            setMedia( *timeline.at(playingIndex) -> info -> url);
            timeline.at(playingIndex)->setStyleSheet("background-color: #ffffff");
        } else {
            setMedia( *timeline.at(0) -> info -> url);
            playingIndex=0;
            timeline.at(0)->setStyleSheet("background-color: #ffffff");
        }
        play();
    }
}

void ThePlayer::playPrev(){
    if(int(timeline.size())>0){
        timeline.at(playingIndex)->setStyleSheet("background-color:#02734A;");
        if(playingIndex>0){
            playingIndex--;
            setMedia( *timeline.at(playingIndex) -> info -> url);
            timeline.at(playingIndex)->setStyleSheet("background-color: #ffffff");
        } else {
            setMedia( *timeline.at(timeline.size()-1) -> info -> url);
            playingIndex=timeline.size()-1;
            timeline.at(playingIndex)->setStyleSheet("background-color: #ffffff");
        }
        play();
    }
}


//play the video if it is paused or pause it if is playing
void ThePlayer::playPause() {
    if( this->state() == 2){
        play();
        timeline.at(playingIndex)->setStyleSheet("background-color: #ffffff");
    }
    else{
        pause();
        timeline.at(playingIndex)->setStyleSheet("background-color:#02734A;");
    }
}

//add a video button to the timeline with the info
void ThePlayer::addToTimeline(TheButtonInfo* info, int width) {
    TheButton *button = new TheButton(chosenWidget,int(timeline.size()),1); //type one because it is a timeline video
    button->connect(button, SIGNAL(removeFromTimeline(int)), this, SLOT (removeFromTimeline(int))); // when clicked, remove button from timeline
    button->setMinimumSize(width,120);
    button->setMaximumSize(width,120);
    timeline.push_back(button);
    chosenLayout->addWidget(button);
    button->init(info);
    if(timeline.size()>0&&QMediaPlayer::mediaStatus()==1) {
        setMedia(*(timeline.at(playingIndex)->info->url));
        play();
        timeline.at(playingIndex)->setStyleSheet("background-color: #ffffff");
    }
    //resize the chosen widget as this holds all the chosen files
    chosenWidget->resize(getTimelineWidth(),120);
}

//remove a button from the timeline with the id
void ThePlayer::removeFromTimeline(int id) {
    const int result = QMessageBox::question(
                NULL,
                QString("Delete video from timeline"),
                QString("You are about to delete a video from the timeline. Do you really want to delete it?"),
                QMessageBox::Yes |
                QMessageBox::No );
    switch( result )
    {
    case QMessageBox::Yes:{
        //you will have to resize the chosenwidget here by reducing the width by the size of the button
        int index=-1;
        //get button from timeline vector with the id
        for(int i=0;i<int(timeline.size());i++){
            if(timeline.at(i) -> id==id){
                index=i;
            }
        }
        //when deleting a video from the timeline if it is currently playing we need to play the next video in the time line
        //delete button
        if(index!=-1){
            timeline.at(index)->deleteLater();
            timeline.erase(timeline.begin()+index);
        }
        chosenWidget->resize(getTimelineWidth(),120);

        if(int(timeline.size())==0){
            setMedia(0);
            playingIndex=0;
        }
        //if it is the last video in the timeline then set the media to the first video
        else if (index==int(timeline.size()) && playingIndex==index) {
            setMedia( *timeline.at(0) -> info -> url);
            playingIndex=0;
            play();
        }
        //need to use index rather than id due to ids constantly changing when deleting from the timeline
        else if(playingIndex==index) {
            setMedia( *timeline.at(playingIndex) -> info -> url);
            play();
        }

      break;
   } default:
        break;
    }
}

void ThePlayer::removeAllFromTimeline() {
    const int result = QMessageBox::question(
                NULL,
                QString("Delete all videos from timeline"),
                QString("You are about to delete all videos from the timeline. Do you really want to delete all the videos?"),
                QMessageBox::Yes |
                QMessageBox::No );
    switch( result )
    {
    case QMessageBox::Yes:{
        timeline.clear();
        while (chosenLayout->count()!=0) {
            QLayoutItem* child = chosenLayout->takeAt(0);
            if(child->widget()!=0) {
                delete child->widget();
            }
            delete child;
        }
        chosenWidget->resize(getTimelineWidth(),120);
        setMedia(0);
        playingIndex=0;
        break;
     } default:
          break;
      }
}

void ThePlayer::toggleAudio() {
    if(volume()==100)
        setVolume(0);
    else
        setVolume(100);
}

int ThePlayer::getTimelineWidth(){
    int width=0;
    for(int i=0;i<int(timeline.size());i++){
        width+=timeline.at(i)->duration+20;
    }
    return width;
}
