//
// Created by twak on 11/11/2019.
//

#ifndef CW2_THE_BUTTON_H
#define CW2_THE_BUTTON_H


#include <QPushButton>
#include <QUrl>
#include <QEvent>
#include <QDate>
#include <QScrollArea>
#include <QLabel>


class TheButtonInfo {

public:
    QUrl* url; // video file to play
    QIcon* icon; // icon to display
    TheButtonInfo ( QUrl* url, QIcon* icon) : url (url), icon (icon) {}
};


class TheButton : public QPushButton {
    Q_OBJECT

public:
    TheButtonInfo* info;
    int id;
    int type; //0 for imported videos and 1 for timeline videos
    int duration; //the duration of the video

    // use these as a handle on the elements so that we can change colour
    QWidget* w;
    QWidget* cont;
    QScrollArea* f;
    QScrollArea* cho;
    QLabel* il;
    QLabel* tl;
    QPushButton* prb;
    QPushButton* pb;
    QPushButton* nb;
    QPushButton* mb;
    QPushButton* bb;
    int accessible = 0;



    QDate date; //date of creation of the video
     TheButton(QWidget *parent, int index,int t) :  QPushButton(parent) {
         setIconSize(QSize(200,110));
         connect(this, SIGNAL(clicked()), this, SLOT (clicked() )); // if QPushButton clicked...then run clicked() below
         id=index;
         type=t;
     }
     //constructor for the theme button
     TheButton(int t,QWidget* window,QWidget* controls, QScrollArea* files, QScrollArea* chosen,QLabel* importl, QLabel* timelinel,QPushButton* prevb,QPushButton* playb, QPushButton* nextb,QPushButton* muteb,QPushButton* binb ) {
         QPushButton();
         type=t;
         w = window;
         f = files;
         cont = controls;
         cho = chosen;
         il = importl;
         tl = timelinel;
         prb = prevb;
         pb = playb;
         nb = nextb;
         mb = muteb;
         bb = binb;
         connect(this, SIGNAL(clicked()), this, SLOT (clicked() )); // if QPushButton clicked...then run clicked() below
     }

    void init(TheButtonInfo* i);

private slots:
    void clicked();

signals:
      void addToTimeline(TheButtonInfo*,int);
      void removeFromTimeline(int);
      void playPrev();
};




#endif //CW2_THE_BUTTON_H
