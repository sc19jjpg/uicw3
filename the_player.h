//
// Created by twak on 11/11/2019.
//

#ifndef CW2_THE_PLAYER_H
#define CW2_THE_PLAYER_H


#include <QApplication>
#include <QMediaPlayer>
#include "the_button.h"
#include <vector>
#include <QTimer>
#include <QtWidgets/QHBoxLayout>
#include <QMessageBox>

using namespace std;

class ThePlayer : public QMediaPlayer {

Q_OBJECT

private:
    vector<TheButtonInfo>* infos;
    vector<TheButton*>* buttons;

    int playingIndex;
    QHBoxLayout *chosenLayout;
    QWidget *chosenWidget;


public:
    vector<TheButton*> timeline;
    ThePlayer() : QMediaPlayer(NULL) {
        playingIndex=0;
//        setVolume(0); // be slightly less annoying
        connect (this, SIGNAL (mediaStatusChanged(QMediaPlayer::MediaStatus)), this, SLOT (playStatusChanged(QMediaPlayer::MediaStatus)) );
    }

    // all buttons have been setup, store pointers here
    void setContent(vector<TheButton*>* b, vector<TheButtonInfo>* i,QHBoxLayout *l,QWidget *w);

private slots:
    void playStatusChanged (QMediaPlayer::MediaStatus ms);

public slots:
    void playPause();
    void playNext();
    void playPrev();
    void addToTimeline(TheButtonInfo* info, int width);
    void removeFromTimeline(int id);
    void removeAllFromTimeline();
    void toggleAudio();
    int getTimelineWidth();
};

#endif //CW2_THE_PLAYER_H
